<?php



function encrypt($plainText,$key)
{
    $secretKey = hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
    $blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
    $plainPad = pkcs5_pad($plainText, $blockSize);
    if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1)
    {
        $encryptedText = mcrypt_generic($openMode, $plainPad);
        mcrypt_generic_deinit($openMode);

    }
    return bin2hex($encryptedText);
}

function decrypt($encryptedText,$key)
{
    $secretKey = hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    $encryptedText=hextobin($encryptedText);
    $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
    mcrypt_generic_init($openMode, $secretKey, $initVector);
    $decryptedText = mdecrypt_generic($openMode, $encryptedText);
    $decryptedText = rtrim($decryptedText, "\0");
    mcrypt_generic_deinit($openMode);
    return $decryptedText;

}
//*********** Padding Function *********************

function pkcs5_pad ($plainText, $blockSize)
{
    $pad = $blockSize - (strlen($plainText) % $blockSize);
    return $plainText . str_repeat(chr($pad), $pad);
}

//********** Hexadecimal to Binary function for php 4.0 version ********

function hextobin($hexString)
{
    $length = strlen($hexString);
    $binString="";
    $count=0;
    while($count<$length)
    {
        $subString =substr($hexString,$count,2);
        $packedString = pack("H*",$subString);
        if ($count==0)
        {
            $binString=$packedString;
        }

        else
        {
            $binString.=$packedString;
        }

        $count+=2;
    }
    return $binString;
}

/**
 * PaymentController
 *
 * The PaymentController class.
 *
 * @package BC
 * @subpackage Ccavenue
 * @author Shahal Tharique <shahalpk@gmail.com>
 *
 */

class BC_Ccavenue_PaymentController extends Mage_Core_Controller_Front_Action {

    public function cancelAction() {
        if ( Mage::getSingleton( 'checkout/session' )->getLastRealOrderId() ) {
            $order = Mage::getModel( 'sales/order' )->loadByIncrementId( Mage::getSingleton( 'checkout/session' )->getLastRealOrderId() );
            if ( $order->getId() ) {
                // Flag the order as 'cancelled' and save it
                $order->cancel()->setState( Mage_Sales_Model_Order::STATE_CANCELED, true, 'CCAvenue has declined the payment.' )->save();
            }
        }
    }

    public function redirectAction(){
        $order = new Mage_Sales_Model_Order();
        $ccd = array();
        $cc_metadata = array();
        $ccd['order_id'] = Mage::getSingleton( 'checkout/session' )->getLastRealOrderId();

        $order->loadByIncrementId( $ccd['order_id'] );

        $cc_metadata["access_code"] = Mage::getStoreConfig( 'payment/bc_ccavenue/access_code' );
        $cc_metadata["encryption_key"] = Mage::getStoreConfig( 'payment/bc_ccavenue/encryption_key' );

        $ccd["merchant_id"] = Mage::getStoreConfig( 'payment/bc_ccavenue/merchant_id');
        $ccd['redirect_url'] = Mage::getUrl("bc_ccavenue/payment/response");
        $ccd['cancel_url'] = Mage::getUrl("bc_ccavenue/payment/response");

        $ccd["amount"] = $order->getGrandTotal();
        $ccd["integration_type"] = "iframe_normal";
        $ccd["language"] = "EN";
        $ccd["currency"] = "INR";

        $billingAddress = $order->getBillingAddress();
        $billingData = $billingAddress->getData();

//        Need to rewrite
        $ccd['billing_name'] = $billingData['firstname'] . ' ' . $billingData['lastname'];
        $ccd['billing_address'] = $billingData["street"];
        $ccd['billing_state'] = $billingData["region"];
        $ccd['billing_country'] = Mage::getModel( 'directory/country' )->load( $billingData["country_id"] )->getName();
        $ccd['billing_tel'] = $billingData["telephone"];
        $ccd['billing_email'] = $order->getCustomerEmail();
        $ccd['billing_city'] = $billingData["city"];
        $ccd['billing_zip'] = $billingData["postcode"];

        $shippingAddress = $order->getShippingAddress();

        if ($shippingAddress)
            $shippingData = $shippingAddress->getData();



        if ( $shippingAddress ) {
            $ccd['delivery_name'] = $shippingData['firstname'] . ' ' . $shippingData['lastname'];
            $ccd['delivery_address'] = $shippingData['street'];
            $ccd['delivery_state'] = $shippingData['region'];
            $ccd['delivery_country'] = Mage::getModel( 'directory/country' )->load( $shippingData['country_id'] )->getName();
            $ccd['delivery_tel'] = $shippingData['telephone'];
            $ccd['delivery_city'] = $shippingData['city'];
            $ccd['delivery_zip'] = $shippingData['postcode'];
        }
        else {
            $ccd['delivery_name'] = '';
            $ccd['delivery_address'] = '';
            $ccd['delivery_state'] = '';
            $ccd['delivery_country'] = '';
            $ccd['delivery_tel'] = '';
            $ccd['delivery_city'] = '';
            $ccd['delivery_zip'] = '';
        }

        $ccd['merchant_param1'] = '';
        $ccd['merchant_param2'] = '';
        $ccd['merchant_param3'] = '';
        $ccd['merchant_param4'] = '';
        $ccd['merchant_param5'] = '';
        $ccd['promo_code'] = '';

        $merchant_data = http_build_query($ccd);
        $encrypted_data = encrypt($merchant_data, $cc_metadata["encryption_key"]);
        Mage::register("encrypted_data", $encrypted_data);
        Mage::register("access_code", $cc_metadata["access_code"]);

        $this->loadLayout();
        $block = $this->getLayout()->createBlock('Mage_Core_Block_Template', 'ccavenue_form', array('template' => 'bc_ccavenue/redirect.phtml'));
        $this->getLayout()->getBlock("content")->append($block);
        $this->renderLayout();
    }

    public function responseAction(){
        if ($this->getRequest()->isPost()){
            $encryption_key = Mage::getStoreConfig( 'payment/bc_ccavenue/encryption_key' );
            $encResp = $this->getRequest()->getParam("encResp");
            $cc_data_qs = trim(decrypt($encResp, $encryption_key));
            $cc_data = array();
            parse_str($cc_data_qs, $cc_data);
            $now = Mage::getModel( 'core/date' )->timestamp( time() );
//            $responselog = Mage::getModel( 'ccavenue/ccavenueresponse' );
//            $responselog->setData($cc_data)
//                ->setCcavenueResponseDtime( date( 'Y-m-d H:i:s', $now ) )
//                ->setCcavenueResponseIp( $this->get_uer_ip() )
//                ->save();


            if ($cc_data["order_status"] == "Success"){
                $order = Mage::getModel( 'sales/order' );
                $order->loadByIncrementId( $cc_data["order_id"] );
                $order->sendNewOrderEmail();
                $order->setState( Mage_Sales_Model_Order::STATE_PROCESSING, true, 'CCAvenue has authorized the payment.' );
                $order->setEmailSent( true );
                $order->save();
                Mage::getSingleton( 'checkout/session' )->unsQuoteId();
                Mage_Core_Controller_Varien_Action::_redirect( 'checkout/onepage/success', array( '_secure' => true ) );
            } else if ($cc_data["order_status"] == "Failure"){
                $this->cancelAction();
                Mage_Core_Controller_Varien_Action::_redirect( 'checkout/onepage/failure', array( '_secure' => true) );
            } else if ($cc_data["order_status"] == "Aborted"){
                $this->cancelAction();
                Mage_Core_Controller_Varien_Action::_redirect( 'checkout/onepage/failure', array( '_secure' => true) );
            } else {		//Some serious error, will log and report to CCAvenue.
                $this->cancelAction();
                Mage_Core_Controller_Varien_Action::_redirect( 'checkout/onepage/failure', array( '_secure' => true) );
            }
        }else{
            $this->_redirect("/");
        }
    }
}