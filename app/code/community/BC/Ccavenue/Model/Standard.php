<?php
/**
 * Main payment model
 *
 * @category    Model
 * @package     BC_Ccavenue
 * @author      Shahal Tharique <shahalpk@gmail.com>
 */

class BC_Ccavenue_Model_Standard extends Mage_Payment_Model_Method_Abstract {
    protected $_code = 'bc_ccavenue';

    protected $_isInitializeNeeded      = true;
    protected $_canUseInternal          = true;
    protected $_canUseForMultishipping  = false;

    public function getOrderPlaceRedirectUrl() {
        return Mage::getUrl( 'ccavenue/payment/redirect', array( '_secure' => true ) );
    }
}
?>